@extends('layouts.app')

{{-- dont ask why it has to be formatted autistic cuz idfk --}}
@section('head')
<script>
		window.Cheats = {!! json_encode(DB::table('cheats')->get()) !!};
	</script>
@endsection

@section('content')
<div class="container py-4">
    <div class="card">
        <div class="card-header">
            Admin
        </div>
        <div class="card-block">				
			<b-card no-body>
				<b-tabs pills card>
					<b-tab title="Active Users" active>
						<b-card>
							<div v-if="items.length > 0">
								<users-table v-cloak :items="items"></users-table>
							</div>
							<div v-else>
								<p>No users logged in or page hasn't been refreshed</p>
							</div>

							<b-button @click="refresh" size="sm">Refresh</b-button>
						</b-card>
					</b-tab>
					<b-tab title="User Management">
						<b-card>
							<table class="table">
								<thead>
									<tr>
										<th scope="col">Id</th>
										<th scope="col">Username</th>
										<th scope="col">Cheats</th>
									</tr>
								</thead>
								<tbody>
								@foreach(DB::table('users')->get() as $user)							 
									<tr>
										<td>{{ $user->id }}</td>
										<td>{{ $user->username }}</td>
										<td>{{ json_decode($user->cheats) }}</td>
									</tr>
								@endforeach
								</tbody>
							</table>
						</b-card>
						
						<b-card>
							<div class="form-row align-items-center">
								<div class="col-auto my-1">
									<label class="mr-sm-2">Give</label>
									<select v-model="selected_user_id" class="custom-select mr-sm-2">
										<option selected>Choose...</option>
										
										@foreach(DB::table('users')->get() as $user)							 
											<option value="{{ $user->id }}">{!! $user->username !!}</option>
										@endforeach
									</select>
								</div>
								<div class="col-auto my-1">
									<select v-model="selected_cheat_id" class="custom-select mr-sm-2">
										<option selected>Choose...</option>
										
										@foreach(DB::table('cheats')->get() as $cheat)							 
											<option value="{{ $cheat->id }}">{!! $cheat->name !!}</option>
										@endforeach
									</select>
								</div>
								<div class="col-auto my-1">
									<b-button variant="primary" @click="give_cheat" class="btn btn-primary">Submit</b-button>
								</div>
							</div>
							<div class="form-row align-items-center">
								<div class="col-auto my-1">
									<label class="mr-sm-2">Remove</label>
									<select v-model="remove_user_id" class="custom-select mr-sm-2">
										<option selected>Choose...</option>
										
										@foreach(DB::table('users')->get() as $user)							 
											<option value="{{ $user->id }}">{!! $user->username !!}</option>
										@endforeach
									</select>
									<label class="mr-sm-2">'s</label>
								</div>
								<div class="col-auto my-1">
									<select v-model="remove_cheat_id" class="custom-select mr-sm-2">
										<option selected>Choose...</option>
										
										@foreach(DB::table('cheats')->get() as $cheat)							 
											<option value="{{ $cheat->id }}">{!! $cheat->name !!}</option>
										@endforeach
									</select>
								</div>
								<div class="col-auto my-1">
									<b-button variant="primary" @click="remove_cheat" class="btn btn-primary">Submit</b-button>
								</div>
							</div>
							
							<small class="form-text text-muted">Refresh to see changes in table.</small>
						</b-card>
						<b-card>
							<form>
							<div class="form-group">
								<label for="a1">Username</label>
								<input v-model="new_username" class="form-control" id="a1" placeholder="Enter Username">
							</div>
							<div class="form-group">
								<label for="b2">Password</label>
								<input v-model="new_password" class="form-control" id="b2" placeholder="Enter Password">
							</div>
								<b-button @click="create_user" variant="primary" class="btn btn-primary">Create User</b-button>
								<small class="form-text text-muted">Refresh to see changes in table.</small>
							</form>
						</b-card>
					</b-tab>
					<b-tab title="Cheat Management">
						<b-card>
							<table class="table">
								<thead>
									<tr>
										<th scope="col">Id</th>
										<th scope="col">Image</th>
										<th scope="col">Name</th>
										<th scope="col">Last Updated</th>
									</tr>
								</thead>
								<tbody>
								@foreach(DB::table('cheats')->get() as $cheat)							 
									<tr>
										<td>{{ $cheat->id }}</td>
										<td>{{ $cheat->image }}</td>
										<td>{{ $cheat->name }}</td>
										<td>{{ $cheat->last_updated }}</td>
									</tr>
								@endforeach
								</tbody>
							</table>
						</b-card>
						
						<label class="mr-sm-2"></label>
						<p>todo: not be lazy and finish this</p>
					</b-tab>
				</b-tabs>
			</b-card>
		</div>
    </div>
</div>
@endsection
