<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cheat extends Model
{	
    protected $fillable = [
		'name'
	];
	
	protected $guarded = [
		'id'
	];
	
	public $timestamps = false;
}
