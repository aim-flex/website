<?php

namespace App\Http\Controllers;

use App\UserSettings;
use Illuminate\Http\Request;

class ServerCommunicationController extends Controller
{
    public function verify()
	{
		return view('verify');
	}
	
	public function update(Request $request)
	{
		$settings = UserSettings::firstOrNew(['username' => $request->input('username')]);
		$settings->settings = base64_decode($request->input('settings'));
		$settings->save();
		
		//return view('blank');
	}
	
	public function get(Request $request)
	{
		$settings = UserSettings::find($request->input('username'));
		
		if ($settings)
			return base64_encode($settings->settings);
		else
			return 'false';
	}
}
