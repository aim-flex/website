<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class InjectionProgressUpdate implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
	
	public $id;
	public $progress;
	
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(int $id, int $progress)
    {
        $this->id = $id;
		$this->progress = $progress;
    }
	
	public function broadcastWith()
	{
		return [
			'data' => $this->progress
		];
	}
	
    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('App.User.' . $this->id);
    }
}
