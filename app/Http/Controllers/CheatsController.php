<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;

class CheatsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {		
        return view('cheats');
    }
	
	public function inject(Request $request)
	{
		if ($request->isMethod('post') && $request->has('id')) 
		{
			$user_cheats = json_decode(\Auth::user()->cheats, true);
			$id = $request->input('id');
			
			if (in_array($id, $user_cheats))
			{
				Redis::publish('requested_cheat', json_encode(
					[\Auth::user()->id, \Auth::user()->username, $id]
				));
			}
		}
	}
}
