<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
		
		DB::table('cheats')->truncate();
		
		DB::table('cheats')->insert([
            'image' => 'csgo_thumbnail.png',
            'name' => 'Counter-Strike: Global Offensive',
            'last_updated' => now(),
        ]);
    }
}
