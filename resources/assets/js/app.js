
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

import Echo from "laravel-echo"

window.io = require('socket.io-client');

window.Echo = new Echo({
	broadcaster: 'socket.io',
	host: window.location.hostname + ':6001',
});

const app = new Vue({
    el: '#app',

    data() {
        return {
			show_alert: false,
			can_inject: true,
			can_close: false,
			alert_variant: 'light',
			cheat_name: '',
			progress: 1,
			progress_max: 100,
		}
    },

    mounted() {		
        window.Echo.private('App.User.' + window.UserID).listen('InjectionProgressUpdate', (e) => {			
			this.update_progress(e.data);
		});
    },

    methods: {
        inject(id, name) {
			this.cheat_name = name;
			this.can_inject = !this.can_inject;
			
			var formdata = new FormData();
			formdata.append('id', id);
			
			axios({
				method: 'post',
				url: '/cheats/inject',
				data: formdata,
				config: { headers: {'Content-Type': 'application/x-www-form-urlencoded' }}
			})
			.then(function (response) {
				//console.log(response);
			})
			.catch(function (response) {
				//console.log(response);
			});
        },
		update_progress(new_progress) {
			this.show_alert = true;
			this.progress = new_progress;
			
			if (new_progress == 100) {
				this.can_close = true;
			}
		},
		enable_injection() {
			this.can_inject = true;
			this.show_alert = false;
		}
    }
});
