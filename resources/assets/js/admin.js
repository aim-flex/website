
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

import Echo from "laravel-echo"

window.io = require('socket.io-client');

window.Echo = new Echo({
	broadcaster: 'socket.io',
	host: window.location.hostname + ':6001',
	//auth: { headers: { 'Authorization': 'Bearer ' + AUTH_API_TOKEN } }
});

Vue.component('users-table', require('./components/UsersTable.vue'));

const app = new Vue({
    el: '#app',

    data() {
        return {
			selected_user_id: -1,
			selected_cheat_id: -1,
			selected_new_username: '',
			selected_new_password: '',
			new_username: '',
			new_password: '',
			remove_user_id: -1,
			remove_cheat_id: -1,
			
			items: []
		}
    },

    mounted() {		
		window.Echo.private('App.User.' + window.UserID).listen('InitialAdminPlayersUpdate', (e) => {			
			var players = JSON.parse(e.data);
			
			var new_players = [];
			
			for (var i in players)
			{
				var name = players[i][0];
				var cheat_id = players[i][1];
					
				var game_name = 'None';
				
				if (cheat_id != 0)
				{
					for (var count in window.Cheats)
					{
						var id = window.Cheats[count].id;
						var window_cheats_name = window.Cheats[count].name;
						
						if (id == cheat_id)
						{
							game_name = window_cheats_name;
						}
					}
				}
				
				new_players.push({Username: name, Game: game_name});
			}
			
			this.items = new_players;
		});
    },

    methods: {
		refresh() {
			axios.get('/admin/refresh');
		},
		give_cheat() {			
			axios.post('/admin/give_cheat', {
				user_id: this.selected_user_id,
				cheat_id: this.selected_cheat_id
			})
			.then(function (response) {
				//console.log(response);
			})
			.catch(function (response) {
				//console.log(response);
			});
		},
		create_user() {
			axios.post('/admin/create_user', {
				username: this.new_username,
				password: this.new_password
			})
			.then(function (response) {
				//console.log(response);
			})
			.catch(function (response) {
				//console.log(response);
			});
		},
		remove_cheat() {
			axios.post('/admin/remove_cheat', {
				user_id: this.remove_user_id,
				cheat_id: this.remove_cheat_id
			})
			.then(function (response) {
				//console.log(response);
			})
			.catch(function (response) {
				//console.log(response);
			});
		}
    }
});
