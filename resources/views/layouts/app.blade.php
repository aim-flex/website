<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>&#65279;</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
		
		window.UserID = {{ Auth::user()->id }}
    </script>
	
	@yield('head')
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container">
                <a class="navbar-brand" href="/">aim-flex</a>
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item {{ Route::currentRouteNamed('home') ? 'active' : '' }}">
                        <a class="nav-link" href="/">Home</a>
                    </li>
                    <li class="nav-item {{ Route::currentRouteNamed('cheats') ? 'active' : '' }}">
                        <a class="nav-link" href="/cheats">Cheats</a>
                    </li>
                </ul>
                <ul class="navbar-nav ml-md-auto">
					@hasrole('owner')
						<li class="nav-item {{ Route::currentRouteNamed('admin') ? 'active' : '' }}">
							<a class="nav-link" href="/admin">Admin</a>
						</li>
					@endhasrole
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            {{ Auth::user()->username }}
                        </a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                Logout
                            </a>
                            
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
        
        @yield('content')
    </div>

    <!-- Scripts -->
	@if (Route::currentRouteNamed('admin'))
		<script src="{{ asset('js/admin.js') }}"></script>
	@else
		<script src="{{ asset('js/app.js') }}"></script>
	@endif
	
	@yield('scripts')
</body>
</html>
