<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>&#65279;</title>
	
    <!-- Styles -->
    <link href="{{ asset('css/login.css') }}" rel="stylesheet">
	<link href="{{ asset('css/app.css') }}" rel="subresource">

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
<body>
    <div id="app">
		<div class="container">
			<form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
				{{ csrf_field() }}
				<input type="text" placeholder="username" name="username" required autofocus>
				<input type="password" placeholder="password" name="password" required>
				<button type="submit" class="btn btn-primary">
					Login
				</button>
			</form>
		</div>
    </div>
	
    <!-- Scripts -->
    <!--<script src="{{ asset('js/app.js') }}"></script>-->
</body>
</html>
