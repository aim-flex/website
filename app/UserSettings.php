<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserSettings extends Model
{
    protected $fillable = [
		'username',
		'settings'
	];
	
	public $timestamps = false;
	protected $primaryKey = 'username';
    public $incrementing = false;
}
