<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;
use Spatie\Permission\Models\Role;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
		$this->middleware(['role:owner']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {		
        return view('admin');
    }
	
	public function refresh(Request $request)
	{
		if (\Auth::check()) {
			Redis::publish('get_initial_admin_players', json_encode(['foo' => 'bar']));
		}
	}
	
	public function give_cheat(Request $request)
	{
		if (\Auth::check() && $request->isMethod('post') && $request->has('user_id') && $request->has('cheat_id'))
		{
			$user_id = $request->input('user_id');
			$cheat_id = $request->input('cheat_id');
			
			$user = \App\User::findOrFail($user_id);
			
			$array = json_decode($user->cheats);
			
			if (!in_array($cheat_id, $array))
			{
				$array[] = (int)$cheat_id;
				
				$user->cheats = json_encode($array);
				$user->save();
			}
		}
	}
	
	public function remove_cheat(Request $request)
	{
		if (\Auth::check() && $request->isMethod('post') && $request->has('user_id') && $request->has('cheat_id'))
		{
			$user_id = $request->input('user_id');
			$cheat_id = $request->input('cheat_id');
			
			$user = \App\User::findOrFail($user_id);
			
			$array = json_decode($user->cheats);
			
			if (in_array($cheat_id, $array))
			{
				$array = array_diff($array, array((int)$cheat_id));
				
				$user->cheats = json_encode($array);
				$user->save();
			}
		}
	}
	
	public function create_user(Request $request)
	{
		if (\Auth::check() && $request->isMethod('post') && $request->has('username') && $request->has('password'))
		{
			$user = new \App\User;
			$user->username = $request->input('username');
			$user->password = \Hash::make($request->input('password'));
			$user->cheats = '[]';
			$user->save();
		}
	}
}
