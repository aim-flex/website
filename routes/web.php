<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('/verify', 'ServerCommunicationController@verify')->name('verify');
Route::post('/update_settings', 'ServerCommunicationController@update')->name('update_settings');
Route::get('/get_settings', 'ServerCommunicationController@get')->name('get_settings');

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::get('/cheats', 'CheatsController@index')->name('cheats');
Route::post('/cheats/inject', 'CheatsController@inject')->name('inject');

Route::get('/admin', 'AdminController@index')->name('admin');
Route::get('/admin/refresh', 'AdminController@refresh')->name('refresh');
Route::post('/admin/give_cheat', 'AdminController@give_cheat')->name('give_cheat');
Route::post('/admin/create_user', 'AdminController@create_user')->name('create_user');
Route::post('/admin/remove_cheat', 'AdminController@remove_cheat')->name('remove_cheat');
