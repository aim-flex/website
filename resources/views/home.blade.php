@extends('layouts.app')

@section('content')
<div class="container py-4">
    <div class="card">
        <div class="card-header">
            Home
        </div>
        <div class="card-block">
            <div class="row">
                <div class="col-md-2">
                    <div class="card-body">
                        <p class="card-text">Logged in</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
