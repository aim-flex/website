<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Log;

class SubscribeRedis extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'redis:subscribe';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Subscribe to the Redis channels';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
		Redis::psubscribe(['*'], function($message, $channel) {
			if ($channel == 'initial_admin_players_update')
			{
				broadcast(new \App\Events\InitialAdminPlayersUpdate($message));
			}
			else if ($channel == 'injection_progress_update')
			{
				$decoded = json_decode($message);
				
				broadcast(new \App\Events\InjectionProgressUpdate($decoded[0], $decoded[1]));
			}
		});
    }
}
