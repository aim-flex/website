@extends('layouts.app')

@section('content')
<div class="container py-4">	
	<div v-cloak>
		<b-alert @dismissed="enable_injection" :show="show_alert" :dismissible="can_close" :variant="alert_variant">
			<div v-if="progress < 100">
				<p>Injecting the <b>@{{ cheat_name }}</b> cheat...</p>
			</div>
			<div v-else>
				<p>Successfully injected the <b>@{{ cheat_name }}</b> cheat!</p>
			</div>
			
			<b-progress class="mt-1" :max="progress_max" animated>
				<b-progress-bar :value="progress" variant="primary"></b-progress-bar>
			</b-progress>
		</b-alert>
	</div>
	
	<div class="card">
        <div class="card-header">
            Cheats
        </div>
        <div class="card-block">			
			@php ($user_cheats = json_decode(Auth::user()->cheats))
			
			{{-- maybe instead use a controller or something? --}}
			@if (count($user_cheats) > 0)	
				<div class="row">
				@foreach($user_cheats as $cheat_index)
					@php ($cheat = DB::table('cheats')->where('id', $cheat_index)->first())
					
					<div class="col-md-3">
						<div class="card-body">                        
							<div class="card">
								<img class="card-img-top" src="images/{{ $cheat->image }}">
								<div class="card-body">
									<h4 class="card-title">{{ $cheat->name }}</h4>
								</div>
								<b-btn :disabled="can_inject ? null : true" class="btn btn-primary btn-lg btn-block" @click="inject({{ $cheat_index }}, '{{ $cheat->name }}')" variant="primary" 
									exact :style="can_inject ? { cursor: 'pointer'} : null">
									Inject
								</b-btn>
								<div class="card-footer">
									<small class="text-muted">Last updated {{ Carbon\Carbon::parse($cheat->last_updated)->diffForHumans() }}</small>
								</div>
							</div>
						</div>
					</div>
				@endforeach
				</div>
			@else
				<div class="card-block">
					<div class="row">
						<div class="col-lg-8">
							<div class="card-body">
								<p class="card-text">You are not subscribed to any cheats</p>
							</div>
						</div>
					</div>
				</div>
			@endif
        </div>
    </div>
</div>
@endsection